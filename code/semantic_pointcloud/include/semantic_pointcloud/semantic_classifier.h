#include <caffe/caffe.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <algorithm>
#include <iosfwd>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <chrono>

namespace semantic_pointcloud
{

class SemanticClassifierParameter
{
private:
    // envrionment box filter parameters
    static constexpr const char* MODEL_FILE = "";
    static constexpr const char* WEIGHTS_FILE = "";
    static constexpr const char* LABEL_FILE = "";
    float R_MEAN = 10;
    float G_MEAN = 10;
    float B_MEAN = 10;
    bool RESIZE_BACK = false;

public:
    SemanticClassifierParameter();
    ~SemanticClassifierParameter();

    std::string model_file;
    std::string weights_file;
    std::string label_file;
    float r_mean;
    float g_mean;
    float b_mean;
    bool resize_back;
};

class SemanticClassifier
{
public:
    SemanticClassifier(const SemanticClassifierParameter& param = SemanticClassifierParameter());
    virtual ~SemanticClassifier();

public:
    void setInputImage(const cv::Mat& img);

    void setParameter(const SemanticClassifierParameter& param);

    void classify(cv::Mat& labeled_img);

private:
    /**
     * load network parameter
     */
    void initialize();

    /**
     *  warp input layer of the network in seperated cv::Mat objects (one per channel)
     */
    void wrapInputLayer(std::vector<cv::Mat>* input_channels);

    /**
     * resize image to fit input layer dimensions
     */
    void preProcess(const cv::Mat& img, std::vector<cv::Mat>* input_channels);

    /**
     * create classifed image
     */
    void postProcess(caffe::Blob<float>* output_layer, const cv::Mat& label_colours, cv::Mat& labeled_img, bool resize = false);

private:
    bool is_init, is_input_set;
    SemanticClassifierParameter param;

    std::shared_ptr<caffe::Net<float> > net;

    std::vector<cv::Mat> input_channels;

    int ch_num;
    cv::Scalar mean;
    cv::Size input_network_size;
    cv::Size input_image_size;

    cv::Mat labeled_img, label_colours;
};

}
