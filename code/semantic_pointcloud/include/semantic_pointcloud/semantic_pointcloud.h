#ifndef SEMANTIC_POINTCLOUD_H_
#define SEMANTIC_POINTCLOUD_H_

#include <ros/ros.h>

#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <sensor_msgs/CameraInfo.h>
#include <std_msgs/Header.h>

#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

#include <tf/transform_listener.h>

#include "semantic_pointcloud/semantic_classifier.h"
#include "semantic_pointcloud/cloud_builder.h"

namespace semantic_pointcloud
{
// pcl typedefs, xyzrgb
typedef pcl::PointXYZRGBA PointT;
typedef pcl::PointCloud<PointT> PointCloud;
typedef PointCloud::Ptr CloudPtr;

class SemanticPointCloud
{
public:
    SemanticPointCloud();
    virtual ~SemanticPointCloud();

    bool init(ros::NodeHandle& nh);

    void update(const ros::Time& time, const ros::Duration& dt);

private:
    void pointcloudProcessing(const cv::Mat& color, const cv::Mat& depth, PointCloud& cloud);

    void imageCallback(const sensor_msgs::ImageConstPtr& msg);
    void depthImageCallback(const sensor_msgs::ImageConstPtr& msg);
    void cameraInfoCallback(const sensor_msgs::CameraInfoConstPtr& msg);

private:
    SemanticClassifier classifier;

    CloudBuilder cloudbuilder;

    image_transport::Subscriber img_sub;
    image_transport::Subscriber depth_img_sub;
    image_transport::Publisher img_pub;

    ros::Subscriber camera_info_sub;
    ros::Publisher cloud_pub;

    bool is_img_updated;
    bool is_depth_updated;
    bool is_K_updated;

    std_msgs::Header img_header;
    cv::Mat img, depth_img, labled_img;

    bool use_cloudl;
    CloudPtr cloud, cloud_filtered;

    tf::TransformListener tfListener;
    std::string dst_frame;
};

}

#endif