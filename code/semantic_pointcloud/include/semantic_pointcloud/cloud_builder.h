#ifndef CLOUD_BUILDER_H_
#define CLOUD_BUILDER_H_

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <opencv2/core/core.hpp>

namespace semantic_pointcloud
{

// pcl typedefs, xyzrgb
typedef pcl::PointXYZRGBA PointT;
typedef pcl::PointCloud<PointT> PointCloud;
typedef PointCloud::Ptr CloudPtr;

// pcl typedefs, xyzrgbl
typedef pcl::PointXYZL PointTl;
typedef pcl::PointCloud<PointTl> PointCloudl;
typedef PointCloudl::Ptr CloudPtrl;

class CloudBuilder
{
public:
    CloudBuilder();
    virtual ~CloudBuilder();

    void setCameraMatrix(const cv::Mat& K);

    void build(const cv::Mat& colors, const cv::Mat& depth, PointCloud& cloud);

private:
    void initalize(size_t width, size_t heigth, PointCloud& cloud);

    void createLookup(size_t width, size_t heigth);

private:
    bool is_K_init;

    cv::Mat K;
    cv::Mat lookupX, lookupY;
    cv::Mat color, depth;
};

}

#endif