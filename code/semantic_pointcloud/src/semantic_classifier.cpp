#include "semantic_pointcloud/semantic_classifier.h"

using namespace semantic_pointcloud;

SemanticClassifierParameter::SemanticClassifierParameter()
    : model_file(MODEL_FILE),
      weights_file(WEIGHTS_FILE),
      label_file(LABEL_FILE),
      r_mean(R_MEAN),
      g_mean(G_MEAN),
      b_mean(B_MEAN)
{
}

SemanticClassifierParameter::~SemanticClassifierParameter()
{
}

SemanticClassifier::SemanticClassifier(const SemanticClassifierParameter& param)
    : is_init(false),
      is_input_set(false),
      param(param)
{
    initialize();
}

SemanticClassifier::~SemanticClassifier()
{
}

void SemanticClassifier::setParameter(const SemanticClassifierParameter& param)
{
    this->param = param;
    initialize();
}

void SemanticClassifier::initialize()
{
    if(param.model_file.empty() || param.weights_file.empty() || param.label_file.empty() )
        return;
    
    // set mode
    caffe::Caffe::set_mode(caffe::Caffe::GPU);

    // load network
    net.reset(new caffe::Net<float>(param.model_file, caffe::TEST) );
    net->CopyTrainedLayersFrom(param.weights_file);

    // load label colors
    label_colours = cv::imread(param.label_file, 1);

    CHECK_EQ(net->num_inputs(), 1) << "Network should have exactly one input.";
    CHECK_EQ(net->num_outputs(), 1) << "Network should have exactly one output.";

    // get input dimensions
    caffe::Blob<float>* input_layer = net->input_blobs()[0];
    ch_num = input_layer->channels();
    CHECK(ch_num == 3 || ch_num == 1) << "Input layer should have 1 or 3 channels.";
    input_network_size = cv::Size(input_layer->width(), input_layer->height());

    // get dataset mean
    mean = cv::Scalar(param.b_mean, param.g_mean, param.r_mean);

    is_init = true;
}

void SemanticClassifier::setInputImage(const cv::Mat& img)
{
    CHECK( is_init ) << "Not initialize, set correct parameters first";

    caffe::Blob<float>* input_layer = net->input_blobs()[0];
    input_layer->Reshape(1, ch_num, input_network_size.height, input_network_size.width);
    net->Reshape();

    // map inputlayer to cv::mat channels
    wrapInputLayer(&input_channels);

    // preprocess image
    input_image_size = img.size();
    preProcess(img, &input_channels);

    is_input_set = true;
}

void SemanticClassifier::classify(cv::Mat& labeled_img)
{
    CHECK( is_input_set ) << "No input set, set input image first";

    // forward pass
    net->Forward();

    // build classified image
    caffe::Blob<float>* output_layer = net->output_blobs()[0];
    postProcess(output_layer, label_colours, labeled_img, param.resize_back);
}

void SemanticClassifier::wrapInputLayer(std::vector<cv::Mat>* input_channels)
{
    caffe::Blob<float>* input_layer = net->input_blobs()[0];
    int width = input_layer->width();
    int height = input_layer->height();
    float* input_data = input_layer->mutable_cpu_data();
    for (size_t i = 0; i < input_layer->channels(); ++i) {
        cv::Mat channel(height, width, CV_32FC1, input_data);
        input_channels->push_back(channel);
        input_data += width * height;
    }
}

void SemanticClassifier::preProcess(const cv::Mat& img, std::vector<cv::Mat>* input_channels)
{
    // Convert the input image to the input image format of the network
    cv::Mat sample;
    if (img.channels() == 3 && ch_num == 1)
        cv::cvtColor(img, sample, cv::COLOR_BGR2GRAY);
    else if (img.channels() == 4 && ch_num == 1)
        cv::cvtColor(img, sample, cv::COLOR_BGRA2GRAY);
    else if (img.channels() == 4 && ch_num == 3)
        cv::cvtColor(img, sample, cv::COLOR_BGRA2BGR);
    else if (img.channels() == 1 && ch_num == 3)
        cv::cvtColor(img, sample, cv::COLOR_GRAY2BGR);
    else
        sample = img;

    // resize
    cv::Mat sample_resized;
    if( sample.size() != input_network_size )
        cv::resize(sample, sample_resized, input_network_size);
    else
        sample_resized = sample;

    // convert to float
    cv::Mat sample_float;
    if( ch_num == 3 )
        sample_resized.convertTo(sample_float, CV_32FC3);
    else
        sample_resized.convertTo(sample_float, CV_32FC1);

    // subtract dataset mean
    sample_float = sample_float - mean;

    // directly write BGR image planes to warped input layer
    cv::split(sample_float, *input_channels);

    CHECK(reinterpret_cast<float*>(input_channels->at(0).data) == net->input_blobs()[0]->cpu_data())
        << "Input channels are not wrapping the input layer of the network.";
}

void SemanticClassifier::postProcess(caffe::Blob<float>* output_layer, const cv::Mat& label_colours, cv::Mat& labeled_img, bool resize)
{
    // combine channels
    cv::Mat merged_output_image = cv::Mat(output_layer->height(), output_layer->width(), CV_32F, const_cast<float *>(output_layer->cpu_data()));
    merged_output_image.convertTo(merged_output_image, CV_8U);
    cv::cvtColor(merged_output_image.clone(), merged_output_image, CV_GRAY2BGR);

    // relabel ouput image
    LUT(merged_output_image, label_colours, labeled_img);

    // resize image 
    if( resize )
        cv::resize(labeled_img, labeled_img, input_image_size, 0.0, 0.0, cv::INTER_NEAREST);
}
