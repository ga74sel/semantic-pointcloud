#include <ros/ros.h>
#include "semantic_pointcloud/semantic_pointcloud.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "semantic_pointcloud");
    ROS_INFO_STREAM("starting semantic_pointcloud...");

    ros::NodeHandle nh("~");
    semantic_pointcloud::SemanticPointCloud semantic_pointcloud;

    if( !semantic_pointcloud.init(nh) ) {
        ROS_ERROR_STREAM("error init segmentation");
        return -1;
    }

    ros::Time time, prev_time;
    ros::Duration dt;

    ros::Rate rate(20);
    while(ros::ok())
    {
        // time update
        time = ros::Time::now();
        dt = time - prev_time;
        prev_time = time;

        semantic_pointcloud.update(time, dt);

        ros::spinOnce();
        rate.sleep();
    }
    return 0;
}