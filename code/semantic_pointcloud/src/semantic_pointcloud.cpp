#include "semantic_pointcloud/semantic_pointcloud.h"

#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/filters/filter.h>
#include <ctime>

using namespace semantic_pointcloud;
namespace enc = sensor_msgs::image_encodings;

SemanticPointCloud::SemanticPointCloud()
    : is_img_updated(false),
      is_depth_updated(false),
      is_K_updated(false),
      use_cloudl(true)
{
}

SemanticPointCloud::~SemanticPointCloud()
{
}

bool SemanticPointCloud::init(ros::NodeHandle& nh)
{
    // get parameter form ros server
    SemanticClassifierParameter param;
    std::string sub_image_name, sub_depth_image_name, pub_image_name, pub_cloud_name, sub_camera_info_name;

    nh.param<std::string>("sub_image_name", sub_image_name, "/kinect2/qhd/image_color");
    nh.param<std::string>("sub_depth_image_name", sub_depth_image_name, "/kinect2/qhd/image_depth_rect");
    nh.param<std::string>("sub_camera_info_name", sub_camera_info_name, "/kinect2/qhd/camera_info");

    nh.param<std::string>("pub_image_name", pub_image_name, "/semantic_image");
    nh.param<std::string>("pub_cloud_name", pub_cloud_name, "/semantic_cloud");

    nh.param<std::string>("dst_frame", dst_frame, "/floor_base_link");

    nh.getParam("model_file", param.model_file);
    nh.getParam("weights_file", param.weights_file);
    nh.getParam("label_file", param.label_file);
    nh.getParam("r_mean", param.r_mean);
    nh.getParam("g_mean", param.g_mean);
    nh.getParam("b_mean", param.b_mean);
    nh.getParam("resize_back", param.resize_back);
    classifier.setParameter(param);

    // setup publisher / subscriber
    cloud_pub = nh.advertise<sensor_msgs::PointCloud2>(pub_cloud_name, 1);
    camera_info_sub = nh.subscribe<sensor_msgs::CameraInfo>(sub_camera_info_name, 1, &SemanticPointCloud::cameraInfoCallback, this);

    std::shared_ptr<image_transport::ImageTransport > image_transport;
    image_transport.reset( new image_transport::ImageTransport(nh) );

    img_sub = image_transport->subscribe(sub_image_name, 1, &SemanticPointCloud::imageCallback, this);
    depth_img_sub = image_transport->subscribe(sub_depth_image_name, 1, &SemanticPointCloud::depthImageCallback, this);
    img_pub = image_transport->advertise(pub_image_name, 1);

    // init pointclouds
    cloud = CloudPtr( new PointCloud() );
    cloud_filtered = CloudPtr( new PointCloud() );

    return true;
}

void SemanticPointCloud::update(const ros::Time& time, const ros::Duration& dt)
{
    if( is_img_updated && is_depth_updated && is_K_updated) {
        is_img_updated = false;
        is_depth_updated = false;

        // classify image
        classifier.setInputImage(img);
        classifier.classify(labled_img);

        // publish image
        cv_bridge::CvImage msg;
        msg.header = img_header;
        msg.encoding = sensor_msgs::image_encodings::TYPE_8UC3;
        msg.image = labled_img;
        img_pub.publish(msg.toImageMsg());

        // build cloud
        pointcloudProcessing(labled_img, depth_img, *cloud);

        // remove nans
        std::vector<int> removedIndices;
        pcl::removeNaNFromPointCloud(*cloud, *cloud_filtered, removedIndices);

        // transform pointcloud to desired frame
        cloud_filtered->header = pcl_conversions::toPCL(img_header);
        if( !dst_frame.empty() ) {
            bool succ;
            try {
                succ = tfListener.waitForTransform(cloud_filtered->header.frame_id, dst_frame.c_str(), ros::Time(0), ros::Duration(1.0));
                if( succ )
                    succ = pcl_ros::transformPointCloud(dst_frame.c_str(), *cloud_filtered, *cloud_filtered, tfListener);
                if( !succ )
                    return;
            }
            catch (tf::TransformException ex) {
                return;
            }
        }
        
        // publish pointcloud
        sensor_msgs::PointCloud2 pc_msg;
        pcl::toROSMsg(*cloud_filtered, pc_msg);
        cloud_pub.publish(pc_msg);
    }
}

void SemanticPointCloud::pointcloudProcessing(const cv::Mat& color, const cv::Mat& depth, PointCloud& cloud)
{
    // resize depth image, to match img
    cv::Mat depth_resized;
    if( color.size() != depth.size() )
        cv::resize(depth, depth_resized, img.size());
    else
        depth_resized = depth;

    // create pointcloud
    cloudbuilder.build(color, depth, cloud);
}

void SemanticPointCloud::imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
    cv_bridge::CvImageConstPtr cv_ptr;
    try {
        cv_ptr = cv_bridge::toCvShare(msg, enc::BGR8);
        img = cv_ptr->image.clone();
        is_img_updated = true;
        img_header = msg->header;
    }
    catch (cv_bridge::Exception& except) {
        ROS_ERROR("SemanticPointCloud::imageCallback: %s", except.what());
    }
}

void SemanticPointCloud::depthImageCallback(const sensor_msgs::ImageConstPtr& msg)
{
    cv_bridge::CvImageConstPtr cv_ptr;
    try {
        cv_ptr = cv_bridge::toCvShare(msg, msg->encoding);
        depth_img = cv_ptr->image.clone();
        is_depth_updated = true;
    }
    catch (cv_bridge::Exception& except) {
        ROS_ERROR("SemanticPointCloud::depthImageCallback: %s", except.what());
    }
}

void SemanticPointCloud::cameraInfoCallback(const sensor_msgs::CameraInfoConstPtr& msg)
{
    if( !is_K_updated ) {
        is_K_updated = true;
        cv::Mat K = cv::Mat(3, 3, CV_32F);
        float* it = K.ptr<float>();
        for( size_t i = 0; i < msg->K.size(); ++i, ++it) {
            *it = msg->K[i];
        }
        cloudbuilder.setCameraMatrix(K);
    }
}

