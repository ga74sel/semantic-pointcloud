#include "semantic_pointcloud/cloud_builder.h"

using namespace semantic_pointcloud;

CloudBuilder::CloudBuilder()
    : is_K_init(false)
{
}

CloudBuilder::~CloudBuilder()
{
}

void CloudBuilder::setCameraMatrix(const cv::Mat& K)
{
    K.copyTo(this->K);
    is_K_init = true;
}

void CloudBuilder::build(const cv::Mat& color, const cv::Mat& depth, PointCloud& cloud)
{
    static const float nanpoint = std::numeric_limits<float>::quiet_NaN();

    if( !is_K_init )
        return;

    if(color.rows != int(cloud.height) || color.cols != int(cloud.width) ) {
        initalize(color.cols, color.rows, cloud );
        createLookup(color.rows, color.cols);
    }
    
    #pragma omp parallel for
    for(int r = 0; r < depth.rows; ++r) {
        PointT* itP = &cloud.points[r*depth.cols];
        const uint16_t *itD = depth.ptr<uint16_t>(r);
        const cv::Vec3b *itC = color.ptr<cv::Vec3b>(r);
        const float y = lookupY.at<float>(0, r);
        const float *itX = lookupX.ptr<float>();

        for(size_t c = 0; c < (size_t)depth.cols; ++c, ++itP, ++itD, ++itC, ++itX) {
            const float depthValue = *itD / 1000.0f;   

            if(*itD == 0) {
                itP->x = itP->y = itP->z = nanpoint;
                itP->rgba = 0;
                continue;
            }
            itP->z = depthValue;
            itP->x = *itX * depthValue;
            itP->y = y * depthValue;
            itP->b = itC->val[0];
            itP->g = itC->val[1];
            itP->r = itC->val[2];
            itP->a = 255;
        }
    }
}

void CloudBuilder::initalize(size_t width, size_t height, PointCloud& cloud)
{
    cloud.height = height;
    cloud.width = width;
    cloud.is_dense = false;
    cloud.points.resize(width*height);
}

void CloudBuilder::createLookup(size_t height, size_t width)
{
    const float fx = 1.0f/K.at<float>(0, 0);
    const float fy = 1.0f/K.at<float>(1, 1);
    const float cx = K.at<float>(0, 2);
    const float cy = K.at<float>(1, 2);
    float *it;

    lookupY = cv::Mat(1, height, CV_32F);
    it = lookupY.ptr<float>();
    for(size_t r = 0; r < height; ++r, ++it) {
        *it = (r - cy)*fy;
    }

    lookupX = cv::Mat(1, width, CV_32F);
    it = lookupX.ptr<float>();
    for(size_t c = 0; c < width; ++c, ++it) {
        *it = (c - cx)*fx;
    }
}
