# Semantic-Pointcloud

## Project description

C++/Caffe based ROS package for fast classification of 3D pointclouds (e.g. of a kinect).   
A pixelwise image classifier is applied to the 2D camera image. 
The classified 3D pointcloud is then obtained via backprojection, using the depth image.  
The [SUN RGB-D dataset](http://rgbd.cs.princeton.edu/) has been used to classify houshold objects (like chairs, tables, shelves) and manipulate them with a robot.

## Setup

* Follow the [SegNet-Tutorial](https://github.com/alexgkendall/SegNet-Tutorial/blob/master/Example_Models/segnet_model_zoo.md) to setup a `caffe` envrionment.
* Download the pretrained models for the [SUN RGB-D dataset](http://rgbd.cs.princeton.edu/) and save them at a desired path.
* Edit the [launch file](code/semantic_pointcloud/launch/semantic_pointcloud.launch) and change the variables to where you have stored your (*.prototxt, *.caffemodel and label.png file)
* Edit the [launch file](code/semantic_pointcloud/launch/semantic_pointcloud.launch) and change the image and depth image topic names to match your 3D sensor

![Alt Text](semantic-pointcloud.png)

